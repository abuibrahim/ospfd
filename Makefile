CFLAGS+= -Wall -Werror -Wstrict-prototypes
CFLAGS+= -Wmissing-prototypes -Wmissing-declarations
CFLAGS+= -Wshadow -Wpointer-arith -Wcast-qual -Wsign-compare
CFLAGS+= -D_GNU_SOURCE
CFLAGS+= $(shell pkg-config --cflags libevent)
LDLIBS+= $(shell pkg-config --libs libevent)
CFLAGS+= $(shell pkg-config --cflags libbsd-overlay)
LDLIBS+= $(shell pkg-config --libs libbsd-overlay)

bindir?= /usr/bin
mandir?= /usr/share/man

.PHONY: all install clean

all: ospfd ospfctl

ospfd:	area.o auth.o control.o database.o hello.o in_cksum.o \
	interface.o iso_cksum.o kroute.o lsack.o lsreq.o lsupdate.o \
	log.o logmsg.o neighbor.o ospfd.o ospfe.o packet.o parse.o \
	printconf.o rde.o rde_lsdb.o rde_spf.o name2id.o \
	imsg.o imsg-buffer.o siphash.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)

ospfctl: ospfctl.o parser.o imsg.o imsg-buffer.o logmsg.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)

parse.c: parse.y

install: ospfd ospfctl
	install -m 0755 -d $(DESTDIR)$(bindir)
	install -m 0755 ospfd $(DESTDIR)$(bindir)
	install -m 0755 ospfctl $(DESTDIR)$(bindir)
	install -m 0755 -d $(DESTDIR)$(mandir)
	install -m 0644 ospfd.8 $(DESTDIR)$(mandir)
	install -m 0644 ospfd.conf.5 $(DESTDIR)$(mandir)
	install -m 0644 ospfctl.8 $(DESTDIR)$(mandir)

clean:
	-rm -f *.o ospfd ospfctl parse.c
