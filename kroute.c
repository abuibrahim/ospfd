/*	$OpenBSD: kroute.c,v 1.107 2016/12/27 09:15:16 jca Exp $ */

/*
 * Copyright (c) 2004 Esben Norby <norby@openbsd.org>
 * Copyright (c) 2003, 2004 Henning Brauer <henning@openbsd.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/tree.h>
#include <sys/uio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <net/route.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>

#include "ospfd.h"
#include "log.h"

struct {
	u_int32_t		rtseq;
	pid_t			pid;
	int			fib_sync;
	int			fib_serial;
	int			fd;
	int			cmd_fd;
	struct event		ev;
	struct event		reload;
	u_int			rdomain;
#define KR_RELOAD_IDLE	0
#define KR_RELOAD_FETCH	1
#define KR_RELOAD_HOLD	2
	int			reload_state;
} kr_state;

struct kroute_node {
	RB_ENTRY(kroute_node)	 entry;
	struct kroute_node	*next;
	struct kroute		 r;
	int			 serial;
};

struct kif_node {
	RB_ENTRY(kif_node)	 entry;
	TAILQ_HEAD(, kif_addr)	 addrs;
	struct kif		 k;
};

void	kr_redist_remove(struct kroute_node *, struct kroute_node *);
int	kr_redist_eval(struct kroute *, struct kroute *);
void	kr_redistribute(struct kroute_node *);
int	kroute_compare(struct kroute_node *, struct kroute_node *);
int	kif_compare(struct kif_node *, struct kif_node *);
int	kr_change_fib(struct kroute_node *, struct kroute *, int, int);
int	kr_delete_fib(struct kroute_node *);

struct kroute_node	*kroute_find(in_addr_t, u_int8_t, u_int8_t);
struct kroute_node	*kroute_matchgw(struct kroute_node *, struct in_addr);
int			 kroute_insert(struct kroute_node *);
int			 kroute_remove(struct kroute_node *);
void			 kroute_clear(void);

struct kif_node		*kif_find(u_short);
struct kif_node		*kif_insert(u_short);
int			 kif_remove(struct kif_node *);
int			 kif_validate(u_short);

struct kroute_node	*kroute_match(in_addr_t);

int		protect_lo(void);
u_int8_t	prefixlen_classful(in_addr_t);

int		send_rtmsg(int, struct kroute *);
int		recv_nlmsg(int);
int		send_rtgenmsg(int, int, int);

void		rtattr_parse(struct rtattr *, int, struct rtattr **, int);
int		rtattr_add(struct nlmsghdr *, size_t, int, void *, size_t);
int		rtattr_add_sub(struct rtattr *, size_t, int, void *, size_t);
int		newlink(struct nlmsghdr *);
int		dellink(struct nlmsghdr *);
int		newaddr(struct nlmsghdr *);
int		deladdr(struct nlmsghdr *);
int		newroute(struct nlmsghdr *);
int		delroute(struct nlmsghdr *);

int		fetchtable(void);
int		fetchifs(void);
int		dispatch_nlmsg(struct nlmsghdr *);
void		kr_fib_reload_timer(int, short, void *);
void		kr_fib_reload_arm_timer(int);

RB_HEAD(kroute_tree, kroute_node)	krt = RB_INITIALIZER(&krt);
RB_PROTOTYPE(kroute_tree, kroute_node, entry, kroute_compare)
RB_GENERATE(kroute_tree, kroute_node, entry, kroute_compare)

RB_HEAD(kif_tree, kif_node)		kit = RB_INITIALIZER(&kit);
RB_PROTOTYPE(kif_tree, kif_node, entry, kif_compare)
RB_GENERATE(kif_tree, kif_node, entry, kif_compare)

int
kif_init(void)
{
	int			opt;

	kr_state.pid = getpid();
	kr_state.rtseq = 1;

	if ((kr_state.cmd_fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0)
		fatal("socket");

	opt = MAX_RTSOCK_BUF;
	if (setsockopt(kr_state.cmd_fd, SOL_SOCKET, SO_RCVBUFFORCE,
	    &opt, sizeof(opt)) < 0) {
		log_warn("kr_init: setsockopt SO_RCVBUFFORCE");
		return (-1);
	}

	if (fetchifs() == -1)
		return (-1);

	return (0);
}

int
kr_init(int fs, u_int rdomain)
{
	struct sockaddr_nl	snl;
	int			opt;

	kr_state.fib_sync = fs;
	kr_state.rdomain = rdomain;

	if ((kr_state.fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0) {
		log_warn("kr_init: socket");
		return (-1);
	}

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;
	snl.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR | RTMGRP_IPV4_ROUTE;
	if (bind(kr_state.fd, (struct sockaddr *)&snl, sizeof(snl)) < 0) {
		log_warn("kr_init: bind");
		return (-1);
	}

	if (fcntl(kr_state.fd, F_SETFL, O_NONBLOCK) < 0) {
		log_warn("kr_init: fcntl");
		return (-1);
	}

	opt = MAX_RTSOCK_BUF;
	if (setsockopt(kr_state.fd, SOL_SOCKET, SO_RCVBUFFORCE,
	    &opt, sizeof(opt)) < 0) {
		log_warn("kr_init: setsockopt SO_RCVBUFFORCE");
		return (-1);
	}

	if (fetchtable() == -1)
		return (-1);

	if (protect_lo() == -1)
		return (-1);

	event_set(&kr_state.ev, kr_state.fd, EV_READ | EV_PERSIST,
	    kr_dispatch_msg, NULL);
	event_add(&kr_state.ev, NULL);

	kr_state.reload_state = KR_RELOAD_IDLE;
	evtimer_set(&kr_state.reload, kr_fib_reload_timer, NULL);

	return (0);
}

int
kr_change_fib(struct kroute_node *kr, struct kroute *kroute, int krcount,
    int action)
{
	int			 i;
	struct kroute_node	*kn, *nkn;

	if (action == RTM_NEWROUTE) {
		/*
		 * First remove all stale multipath routes.
		 * This step must be skipped when the action is RTM_CHANGE
		 * because it is already a single path route that will be
		 * changed.
		 */
		for (kn = kr; kn != NULL; kn = nkn) {
			for (i = 0; i < krcount; i++) {
				if (kn->r.nexthop.s_addr ==
				    kroute[i].nexthop.s_addr)
					break;
			}
			nkn = kn->next;
			if (i == krcount) {
				/* stale route */
				if (kr_delete_fib(kn) == -1)
					log_warnx("kr_delete_fib failed");
				/*
				 * if head element was removed we need to adjust
				 * the head
				 */
				if (kr == kn)
					kr = nkn;
			}
		}
	}

	/*
	 * now add or change the route
	 */
	for (i = 0; i < krcount; i++) {
		/* nexthop within 127/8 -> ignore silently */
		if ((kroute[i].nexthop.s_addr & htonl(IN_CLASSA_NET)) ==
		    htonl(INADDR_LOOPBACK & IN_CLASSA_NET))
			continue;

		if (action == RTM_NEWROUTE && kr) {
			for (kn = kr; kn != NULL; kn = kn->next) {
				if (kn->r.nexthop.s_addr ==
				    kroute[i].nexthop.s_addr)
					break;
			}

			if (kn != NULL)
				/* nexthop already present, skip it */
				continue;
		} else
			/* modify first entry */
			kn = kr;

		/* send update */
		if (send_rtmsg(action, &kroute[i]) == -1)
			return (-1);

		/* create new entry unless we are changing the first entry */
		if (action == RTM_NEWROUTE)
			if ((kn = calloc(1, sizeof(*kn))) == NULL)
				fatal(NULL);

		kn->r.prefix.s_addr = kroute[i].prefix.s_addr;
		kn->r.prefixlen = kroute[i].prefixlen;
		kn->r.nexthop.s_addr = kroute[i].nexthop.s_addr;
		kn->r.flags = kroute[i].flags | F_OSPFD_INSERTED;
		kn->r.priority = RTPROT_OSPF;
		kn->r.ext_tag = kroute[i].ext_tag;
		rtlabel_unref(kn->r.rtlabel);	/* for RTM_CHANGE */
		kn->r.rtlabel = kroute[i].rtlabel;

		if (action == RTM_NEWROUTE)
			if (kroute_insert(kn) == -1) {
				log_warnx("kr_update_fib: cannot insert %s",
				    inet_ntoa(kn->r.nexthop));
				free(kn);
			}
		action = RTM_NEWROUTE;
	}
	return  (0);
}

int
kr_change(struct kroute *kroute, int krcount)
{
	struct kroute_node	*kr;
	int			 action = RTM_NEWROUTE;

	kroute->rtlabel = rtlabel_tag2id(kroute->ext_tag);

	kr = kroute_find(kroute->prefix.s_addr, kroute->prefixlen, RTPROT_OSPF);
	if (kr != NULL && kr->next == NULL && krcount == 1)
		/* single path OSPF route */
		action = 0;

	return (kr_change_fib(kr, kroute, krcount, action));
}

int
kr_delete_fib(struct kroute_node *kr)
{
	if (kr->r.priority != RTPROT_OSPF)
		log_warn("kr_delete_fib: %s/%d has wrong priority %d",
		    inet_ntoa(kr->r.prefix), kr->r.prefixlen, kr->r.priority);

	if (send_rtmsg(RTM_DELROUTE, &kr->r) == -1)
		return (-1);

	if (kroute_remove(kr) == -1)
		return (-1);

	return (0);
}

int
kr_delete(struct kroute *kroute)
{
	struct kroute_node	*kr, *nkr;

	kr = kroute_find(kroute->prefix.s_addr, kroute->prefixlen, RTPROT_OSPF);
	if (kr == NULL)
		return (0);

	while (kr != NULL) {
		nkr = kr->next;
		if (kr_delete_fib(kr) == -1)
			return (-1);
		kr = nkr;
	}
	return (0);
}

void
kr_shutdown(void)
{
	kr_fib_decouple();
	kroute_clear();
	kif_clear();
}

void
kr_fib_couple(void)
{
	struct kroute_node	*kr;
	struct kroute_node	*kn;

	if (kr_state.fib_sync == 1)	/* already coupled */
		return;

	kr_state.fib_sync = 1;

	RB_FOREACH(kr, kroute_tree, &krt)
		if (kr->r.priority == RTPROT_OSPF)
			for (kn = kr; kn != NULL; kn = kn->next)
				if (send_rtmsg(RTM_NEWROUTE, &kn->r) < 0)
					log_warn("kr_fib_couple: send_rtmsg");

	log_info("kernel routing table coupled");
}

void
kr_fib_decouple(void)
{
	struct kroute_node	*kr;
	struct kroute_node	*kn;

	if (kr_state.fib_sync == 0)	/* already decoupled */
		return;

	RB_FOREACH(kr, kroute_tree, &krt)
		if (kr->r.priority == RTPROT_OSPF)
			for (kn = kr; kn != NULL; kn = kn->next)
				if (send_rtmsg(RTM_DELROUTE, &kn->r) < 0)
					log_warn("kr_fib_decouple: send_rtmsg");

	kr_state.fib_sync = 0;

	log_info("kernel routing table decoupled");
}

void
kr_fib_reload_timer(int fd, short event, void *bula)
{
	if (kr_state.reload_state == KR_RELOAD_FETCH) {
		kr_fib_reload();
		kr_state.reload_state = KR_RELOAD_HOLD;
		kr_fib_reload_arm_timer(KR_RELOAD_HOLD_TIMER);
	} else {
		kr_state.reload_state = KR_RELOAD_IDLE;
	}
}

void
kr_fib_reload_arm_timer(int delay)
{
	struct timeval		 tv;

	timerclear(&tv);
	tv.tv_sec = delay / 1000;
	tv.tv_usec = (delay % 1000) * 1000;

	if (evtimer_add(&kr_state.reload, &tv) == -1)
		fatal("add_reload_timer");
}

void
kr_fib_reload()
{
	struct kroute_node	*krn, *kr, *kn;

	log_info("reloading interface list and routing table");

	kr_state.fib_serial++;

	if (fetchifs() == -1 || fetchtable() == -1)
		return;

	for (kr = RB_MIN(kroute_tree, &krt); kr != NULL; kr = krn) {
		krn = RB_NEXT(kroute_tree, &krt, kr);

		do {
			kn = kr->next;

			if (kr->serial != kr_state.fib_serial) {
				if (kr->r.priority == RTPROT_OSPF) {
					kr->serial = kr_state.fib_serial;
					if (send_rtmsg(RTM_NEWROUTE, &kr->r) != 0)
						break;
				} else
					kroute_remove(kr);
			}

		} while ((kr = kn) != NULL);
	}
}

/* ARGSUSED */
void
kr_dispatch_msg(int fd, short event, void *bula)
{
	if (recv_nlmsg(fd) == -1)
		log_warn("kr_dispatch_msg: recv_nlmsg");
}

void
kr_show_route(struct imsg *imsg)
{
	struct kroute_node	*kr;
	struct kroute_node	*kn;
	int			 flags;
	struct in_addr		 addr;

	switch (imsg->hdr.type) {
	case IMSG_CTL_KROUTE:
		if (imsg->hdr.len != IMSG_HEADER_SIZE + sizeof(flags)) {
			log_warnx("kr_show_route: wrong imsg len");
			return;
		}
		memcpy(&flags, imsg->data, sizeof(flags));
		RB_FOREACH(kr, kroute_tree, &krt)
			if (!flags || kr->r.flags & flags) {
				kn = kr;
				do {
					main_imsg_compose_ospfe(IMSG_CTL_KROUTE,
					    imsg->hdr.pid,
					    &kn->r, sizeof(kn->r));
				} while ((kn = kn->next) != NULL);
			}
		break;
	case IMSG_CTL_KROUTE_ADDR:
		if (imsg->hdr.len != IMSG_HEADER_SIZE +
		    sizeof(struct in_addr)) {
			log_warnx("kr_show_route: wrong imsg len");
			return;
		}
		memcpy(&addr, imsg->data, sizeof(addr));
		kr = NULL;
		kr = kroute_match(addr.s_addr);
		if (kr != NULL)
			main_imsg_compose_ospfe(IMSG_CTL_KROUTE, imsg->hdr.pid,
			    &kr->r, sizeof(kr->r));
		break;
	default:
		log_warnx("kr_show_route: error handling imsg");
		break;
	}

	main_imsg_compose_ospfe(IMSG_CTL_END, imsg->hdr.pid, NULL, 0);
}

void
kr_ifinfo(char *ifname, pid_t pid)
{
	struct kif_node	*kif;

	RB_FOREACH(kif, kif_tree, &kit)
		if (ifname == NULL || !strcmp(ifname, kif->k.ifname)) {
			main_imsg_compose_ospfe(IMSG_CTL_IFINFO,
			    pid, &kif->k, sizeof(kif->k));
		}

	main_imsg_compose_ospfe(IMSG_CTL_END, pid, NULL, 0);
}

void
kr_redist_remove(struct kroute_node *kh, struct kroute_node *kn)
{
	struct kroute	*kr;

	/* was the route redistributed? */
	if ((kn->r.flags & F_REDISTRIBUTED) == 0)
		return;

	/* remove redistributed flag */
	kn->r.flags &= ~F_REDISTRIBUTED;
	kr = &kn->r;

	/* probably inform the RDE (check if no other path is redistributed) */
	for (kn = kh; kn; kn = kn->next)
		if (kn->r.flags & F_REDISTRIBUTED)
			break;

	if (kn == NULL)
		main_imsg_compose_rde(IMSG_NETWORK_DEL, 0, kr,
		    sizeof(struct kroute));
}

int
kr_redist_eval(struct kroute *kr, struct kroute *new_kr)
{
	u_int32_t	 a, metric = 0;

	/* Only non-ospfd routes are considered for redistribution. */
	if (!(kr->flags & F_KERNEL))
		goto dont_redistribute;

	/* Dynamic routes are not redistributable. */
	if (kr->flags & F_DYNAMIC)
		goto dont_redistribute;

	/* interface is not up and running so don't announce */
	if (kr->flags & F_DOWN)
		goto dont_redistribute;

	/*
	 * We consider the loopback net, multicast and experimental addresses
	 * as not redistributable.
	 */
	a = ntohl(kr->prefix.s_addr);
	if (IN_MULTICAST(a) || IN_BADCLASS(a) ||
	    (a >> IN_CLASSA_NSHIFT) == IN_LOOPBACKNET)
		goto dont_redistribute;
	/*
	 * Consider networks with nexthop loopback as not redistributable
	 * unless it is a reject or blackhole route.
	 */
	if (kr->nexthop.s_addr == htonl(INADDR_LOOPBACK) &&
	    !(kr->flags & (F_BLACKHOLE|F_REJECT)))
		goto dont_redistribute;

	/* Should we redistribute this route? */
	if (!ospf_redistribute(kr, &metric))
		goto dont_redistribute;

	/* prefix should be redistributed */
	kr->flags |= F_REDISTRIBUTED;
	/*
	 * only one of all multipath routes can be redistributed so
	 * redistribute the best one.
	 */
	if (new_kr->metric > metric) {
		*new_kr = *kr;
		new_kr->metric = metric;
	}

	return (1);

dont_redistribute:
	/* was the route redistributed? */
	if ((kr->flags & F_REDISTRIBUTED) == 0)
		return (0);

	kr->flags &= ~F_REDISTRIBUTED;
	return (1);
}

void
kr_redistribute(struct kroute_node *kh)
{
	struct kroute_node	*kn;
	struct kroute		 kr;
	int			 redistribute = 0;

	/* only the highest prio route can be redistributed */
	if (kroute_find(kh->r.prefix.s_addr, kh->r.prefixlen, RTPROT_UNSPEC) != kh)
		return;

	bzero(&kr, sizeof(kr));
	kr.metric = UINT_MAX;
	for (kn = kh; kn; kn = kn->next)
		if (kr_redist_eval(&kn->r, &kr))
			redistribute = 1;

	if (!redistribute)
		return;

	if (kr.flags & F_REDISTRIBUTED) {
		main_imsg_compose_rde(IMSG_NETWORK_ADD, 0, &kr,
		    sizeof(struct kroute));
	} else {
		kr = kh->r;
		main_imsg_compose_rde(IMSG_NETWORK_DEL, 0, &kr,
		    sizeof(struct kroute));
	}
}

void
kr_reload(void)
{
	struct kroute_node	*kr, *kn;
	u_int32_t		 dummy;
	int			 r;

	RB_FOREACH(kr, kroute_tree, &krt) {
		for (kn = kr; kn; kn = kn->next) {
			r = ospf_redistribute(&kn->r, &dummy);
			/*
			 * if it is redistributed, redistribute again metric
			 * may have changed.
			 */
			if ((kn->r.flags & F_REDISTRIBUTED && !r) || r)
				break;
		}
		if (kn) {
			/*
			 * kr_redistribute copes with removes and RDE with
			 * duplicates
			 */
			kr_redistribute(kr);
		}
	}
}

/* rb-tree compare */
int
kroute_compare(struct kroute_node *a, struct kroute_node *b)
{
	if (ntohl(a->r.prefix.s_addr) < ntohl(b->r.prefix.s_addr))
		return (-1);
	if (ntohl(a->r.prefix.s_addr) > ntohl(b->r.prefix.s_addr))
		return (1);
	if (a->r.prefixlen < b->r.prefixlen)
		return (-1);
	if (a->r.prefixlen > b->r.prefixlen)
		return (1);

	/* if the priority is RTPROT_UNSPEC finish on the first address hit */
	if (a->r.priority == RTPROT_UNSPEC || b->r.priority == RTPROT_UNSPEC)
		return (0);
	if (a->r.priority < b->r.priority)
		return (-1);
	if (a->r.priority > b->r.priority)
		return (1);
	return (0);
}

int
kif_compare(struct kif_node *a, struct kif_node *b)
{
	return (b->k.ifindex - a->k.ifindex);
}

/* tree management */
struct kroute_node *
kroute_find(in_addr_t prefix, u_int8_t prefixlen, u_int8_t prio)
{
	struct kroute_node	s;
	struct kroute_node	*kn, *tmp;

	s.r.prefix.s_addr = prefix;
	s.r.prefixlen = prefixlen;
	s.r.priority = prio;

	kn = RB_FIND(kroute_tree, &krt, &s);
	if (kn && prio == RTPROT_UNSPEC) {
		tmp = RB_PREV(kroute_tree, &krt, kn);
		while (tmp) {
			if (kroute_compare(&s, tmp) == 0)
				kn = tmp;
			else
				break;
			tmp = RB_PREV(kroute_tree, &krt, kn);
		}
	}
	return (kn);
}

struct kroute_node *
kroute_matchgw(struct kroute_node *kr, struct in_addr nh)
{
	in_addr_t	nexthop;

	nexthop = nh.s_addr;

	while (kr) {
		if (kr->r.nexthop.s_addr == nexthop)
			return (kr);
		kr = kr->next;
	}

	return (NULL);
}

int
kroute_insert(struct kroute_node *kr)
{
	struct kroute_node	*krm, *krh;

	kr->serial = kr_state.fib_serial;

	if ((krh = RB_INSERT(kroute_tree, &krt, kr)) != NULL) {
		/*
		 * Multipath route, add at end of list.
		 */
		krm = krh;
		while (krm->next != NULL)
			krm = krm->next;
		krm->next = kr;
		kr->next = NULL; /* to be sure */
	} else
		krh = kr;

	if (!(kr->r.flags & F_KERNEL)) {
		/* don't validate or redistribute ospf route */
		kr->r.flags &= ~F_DOWN;
		return (0);
	}

	if (kif_validate(kr->r.ifindex))
		kr->r.flags &= ~F_DOWN;
	else
		kr->r.flags |= F_DOWN;

	kr_redistribute(krh);
	return (0);
}

int
kroute_remove(struct kroute_node *kr)
{
	struct kroute_node	*krm;

	if ((krm = RB_FIND(kroute_tree, &krt, kr)) == NULL) {
		log_warnx("kroute_remove failed to find %s/%u",
		    inet_ntoa(kr->r.prefix), kr->r.prefixlen);
		return (-1);
	}

	if (krm == kr) {
		/* head element */
		if (RB_REMOVE(kroute_tree, &krt, kr) == NULL) {
			log_warnx("kroute_remove failed for %s/%u",
			    inet_ntoa(kr->r.prefix), kr->r.prefixlen);
			return (-1);
		}
		if (kr->next != NULL) {
			if (RB_INSERT(kroute_tree, &krt, kr->next) != NULL) {
				log_warnx("kroute_remove failed to add %s/%u",
				    inet_ntoa(kr->r.prefix), kr->r.prefixlen);
				return (-1);
			}
		}
	} else {
		/* somewhere in the list */
		while (krm->next != kr && krm->next != NULL)
			krm = krm->next;
		if (krm->next == NULL) {
			log_warnx("kroute_remove multipath list corrupted "
			    "for %s/%u", inet_ntoa(kr->r.prefix),
			    kr->r.prefixlen);
			return (-1);
		}
		krm->next = kr->next;
	}

	kr_redist_remove(krm, kr);
	rtlabel_unref(kr->r.rtlabel);

	free(kr);
	return (0);
}

void
kroute_clear(void)
{
	struct kroute_node	*kr;

	while ((kr = RB_MIN(kroute_tree, &krt)) != NULL)
		kroute_remove(kr);
}

struct kif_node *
kif_find(u_short ifindex)
{
	struct kif_node	s;

	bzero(&s, sizeof(s));
	s.k.ifindex = ifindex;

	return (RB_FIND(kif_tree, &kit, &s));
}

struct kif *
kif_findname(char *ifname, struct in_addr addr, struct kif_addr **kap)
{
	struct kif_node	*kif;
	struct kif_addr	*ka;

	RB_FOREACH(kif, kif_tree, &kit)
		if (!strcmp(ifname, kif->k.ifname)) {
			ka = TAILQ_FIRST(&kif->addrs);
			if (addr.s_addr != 0) {
				TAILQ_FOREACH(ka, &kif->addrs, entry) {
					if (addr.s_addr == ka->addr.s_addr)
						break;
				}
			}
			if (kap != NULL)
				*kap = ka;
			return (&kif->k);
		}

	return (NULL);
}

struct kif_node *
kif_insert(u_short ifindex)
{
	struct kif_node	*kif;

	if ((kif = calloc(1, sizeof(struct kif_node))) == NULL)
		return (NULL);

	kif->k.ifindex = ifindex;
	TAILQ_INIT(&kif->addrs);

	if (RB_INSERT(kif_tree, &kit, kif) != NULL)
		fatalx("kif_insert: RB_INSERT");

	return (kif);
}

int
kif_remove(struct kif_node *kif)
{
	struct kif_addr	*ka;

	if (RB_REMOVE(kif_tree, &kit, kif) == NULL) {
		log_warnx("RB_REMOVE(kif_tree, &kit, kif)");
		return (-1);
	}

	while ((ka = TAILQ_FIRST(&kif->addrs)) != NULL) {
		TAILQ_REMOVE(&kif->addrs, ka, entry);
		free(ka);
	}
	free(kif);
	return (0);
}

void
kif_clear(void)
{
	struct kif_node	*kif;

	while ((kif = RB_MIN(kif_tree, &kit)) != NULL)
		kif_remove(kif);
}

int
kif_validate(u_short ifindex)
{
	struct kif_node		*kif;

	if ((kif = kif_find(ifindex)) == NULL) {
		log_warnx("interface with index %u not found", ifindex);
		return (1);
	}

	return (kif->k.nh_reachable);
}

struct kroute_node *
kroute_match(in_addr_t key)
{
	int			 i;
	struct kroute_node	*kr;

	/* we will never match the default route */
	for (i = 32; i > 0; i--) {
		kr = kroute_find(key & prefixlen2mask(i), i, RTPROT_UNSPEC);
		if (kr != NULL)
			return (kr);
	}

	/* if we don't have a match yet, try to find a default route */
	if ((kr = kroute_find(0, 0, RTPROT_UNSPEC)) != NULL)
			return (kr);

	return (NULL);
}

/* misc */
int
protect_lo(void)
{
	struct kroute_node	*kr;

	/* special protection for 127/8 */
	if ((kr = calloc(1, sizeof(struct kroute_node))) == NULL) {
		log_warn("protect_lo");
		return (-1);
	}
	kr->r.prefix.s_addr = htonl(INADDR_LOOPBACK & IN_CLASSA_NET);
	kr->r.prefixlen = 8;
	kr->r.flags = F_KERNEL|F_CONNECTED;

	if (RB_INSERT(kroute_tree, &krt, kr) != NULL)
		free(kr);	/* kernel route already there, no problem */

	return (0);
}

u_int8_t
prefixlen_classful(in_addr_t ina)
{
	/* it hurt to write this. */

	if (ina >= 0xf0000000U)		/* class E */
		return (32);
	else if (ina >= 0xe0000000U)	/* class D */
		return (4);
	else if (ina >= 0xc0000000U)	/* class C */
		return (24);
	else if (ina >= 0x80000000U)	/* class B */
		return (16);
	else				/* class A */
		return (8);
}

u_int8_t
mask2prefixlen(in_addr_t ina)
{
	if (ina == 0)
		return (0);
	else
		return (33 - ffs(ntohl(ina)));
}

in_addr_t
prefixlen2mask(u_int8_t prefixlen)
{
	if (prefixlen == 0)
		return (0);

	return (htonl(0xffffffff << (32 - prefixlen)));
}

void
rtattr_parse(struct rtattr *rta, int len, struct rtattr *tb[], int max)
{
	bzero(tb, sizeof (struct rtattr *) * (max + 1));
	for (; RTA_OK(rta, len); rta = RTA_NEXT(rta, len)) {
		if (rta->rta_type <= max)
			tb[rta->rta_type] = rta;
	}
}

static inline void
rtattr_parse_nested(struct rtattr *rta, struct rtattr *tb[], int max)
{
	rtattr_parse(RTA_DATA(rta), RTA_PAYLOAD(rta), tb, max);
}

int
rtattr_add(struct nlmsghdr *n, size_t maxlen, int type, void *data, size_t len)
{
	struct rtattr		*rta;
	size_t			 rlen;

	rlen = RTA_LENGTH(len);
	if (NLMSG_ALIGN(n->nlmsg_len) + rlen > maxlen)
		return (-1);

	rta = (struct rtattr *)((char *)n + NLMSG_ALIGN(n->nlmsg_len));
	rta->rta_type = type;
	rta->rta_len = rlen;
	memcpy(RTA_DATA(rta), data, len);
	n->nlmsg_len = NLMSG_ALIGN(n->nlmsg_len) + rlen;

	return (0);
}

int
rtattr_add_sub(struct rtattr *rta, size_t maxlen, int type, void *data,
    size_t len)
{
	struct rtattr		*subrta;
	size_t			 rlen;

	rlen = RTA_LENGTH(len);
	if (RTA_ALIGN(rta->rta_len) + rlen > maxlen)
		return (-1);

	subrta = (struct rtattr *)((char *)rta + RTA_ALIGN(rta->rta_len));
	subrta->rta_type = type;
	subrta->rta_len = rlen;
	memcpy(RTA_DATA(subrta), data, len);
	rta->rta_len = RTA_ALIGN(rta->rta_len) + rlen;

	return (0);
}

static inline uint32_t
rtattr_get_u32(struct rtattr *rta)
{
	uint32_t	*data = RTA_DATA(rta);

	return rta ? *data : 0;
}

int
newaddr(struct nlmsghdr *nlmsg)
{
	struct rtattr		*tb[IFA_MAX + 1];
	struct rtattr		*label;
	struct ifaddrmsg	*ifa;
	struct kif_node		*kif;
	struct kif_addr		*ka;
	struct in_addr		 addr;
	struct in_addr		 brd;
	int			 len;
	int			 ifindex;

	len = nlmsg->nlmsg_len - NLMSG_LENGTH(sizeof(*ifa));
	if (len < 0)
		return (-1);

	ifa = NLMSG_DATA(nlmsg);
	if (ifa->ifa_family != AF_INET)
		return 0;

	rtattr_parse(IFA_RTA(ifa), len, tb, IFA_MAX);

	ifindex = ifa->ifa_index;
	if ((kif = kif_find(ifindex)) == NULL) {
		log_warnx("newaddr: interface %d not found", ifindex);
		return (-1);
	}

	if (tb[IFA_LOCAL] == NULL)
		tb[IFA_LOCAL] = tb[IFA_ADDRESS];

	if (tb[IFA_ADDRESS] == NULL)
		tb[IFA_ADDRESS] = tb[IFA_LOCAL];

	addr.s_addr = rtattr_get_u32(tb[IFA_LOCAL]);
	if (addr.s_addr == INADDR_ANY)
		return (-1);

	/* is there peer address */
	if (tb[IFA_ADDRESS] &&
	    memcmp(RTA_DATA(tb[IFA_ADDRESS]), RTA_DATA(tb[IFA_LOCAL]),
		RTA_PAYLOAD(tb[IFA_ADDRESS]))) {
		brd.s_addr = rtattr_get_u32(tb[IFA_ADDRESS]);
	} else {
		brd.s_addr = rtattr_get_u32(tb[IFA_BROADCAST]);
	}

	/* check ifa->ifa_flags & IFA_F_SECONDARY */

	label = tb[IFA_LABEL];
	if (label != NULL)
		strlcpy(kif->k.ifname, RTA_DATA(label), sizeof(kif->k.ifname));

	if ((ka = calloc(1, sizeof(struct kif_addr))) == NULL)
		fatal("newaddr: calloc");

	ka->addr = addr;
	ka->mask.s_addr = prefixlen2mask(ifa->ifa_prefixlen);
	ka->dstbrd = brd;

	TAILQ_INSERT_TAIL(&kif->addrs, ka, entry);

	return (0);
}

int
deladdr(struct nlmsghdr *nlmsg)
{
	struct rtattr		*tb[IFA_MAX + 1];
	struct ifaddrmsg	*ifa;
	struct ifaddrdel	 ifc;
	struct kif_node		*kif;
	struct kif_addr		*ka, *nka;
	struct in_addr		 addr;
	int			 len;
	int			 ifindex;

	len = nlmsg->nlmsg_len - NLMSG_LENGTH(sizeof(*ifa));
	if (len < 0)
		return (-1);

	ifa = NLMSG_DATA(nlmsg);
	if (ifa->ifa_family != AF_INET)
		return 0;

	rtattr_parse(IFA_RTA(ifa), len, tb, IFA_MAX);

	ifindex = ifa->ifa_index;
	if ((kif = kif_find(ifindex)) == NULL) {
		log_warnx("deladdr: interface %d not found", ifindex);
		return (-1);
	}

	addr.s_addr = rtattr_get_u32(tb[IFA_ADDRESS]);
	if (addr.s_addr == INADDR_ANY) {
		log_warnx("deladdr: missing IFA_ADDRESS attribute");
		return (-1);
	}

	for (ka = TAILQ_FIRST(&kif->addrs); ka != NULL; ka = nka) {
		nka = TAILQ_NEXT(ka, entry);

		if (ka->addr.s_addr == addr.s_addr) {
			TAILQ_REMOVE(&kif->addrs, ka, entry);
			ifc.addr = addr;
			ifc.ifindex = ifindex;
			main_imsg_compose_ospfe(IMSG_IFADDRDEL, 0, &ifc,
			    sizeof(ifc));
			free(ka);
			return (0);
		}
	}

	return (0);
}

int
newlink(struct nlmsghdr *nlmsg)
{
	struct rtattr		*tb[IFLA_MAX + 1];
	struct ifinfomsg	*ifi;
	struct kroute_node	*kr, *tkr;
	struct kif_node		*kif;
	char			*ifname;
	int			 ifindex, flags, len;
	u_int8_t		 reachable;

	len = nlmsg->nlmsg_len - NLMSG_LENGTH(sizeof(*ifi));
	if (len < 0)
		return (-1);

	ifi = NLMSG_DATA(nlmsg);
	rtattr_parse(IFLA_RTA(ifi), len, tb, IFLA_MAX);

	if ((tb[IFLA_WIRELESS] != NULL) && (ifi->ifi_change == 0)) {
		log_warnx("newlink: ignoring IFLA_WIRELESS attribute");
		return (0);
	}

	if (tb[IFLA_IFNAME] == NULL) {
		log_warnx("newlink: missing IFLA_IFNAME attribute");
		return (-1);
	}

	ifname = RTA_DATA(tb[IFLA_IFNAME]);
	ifindex = ifi->ifi_index;

	if ((kif = kif_find(ifindex)) == NULL) {
		if ((kif = kif_insert(ifindex)) == NULL) {
			log_warn("newlink: failed inserting interface %d",
			    ifindex);
			return (-1);
		}
	}

	strlcpy(kif->k.ifname, ifname, sizeof(kif->k.ifname));
	kif->k.if_type = ifi->ifi_type;
	kif->k.rdomain = 0;
	kif->k.mtu = rtattr_get_u32(tb[IFLA_MTU]);
	kif->k.flags = flags = ifi->ifi_flags;

	/* notify ospfe about interface link state */
	main_imsg_compose_ospfe(IMSG_IFINFO, 0, kif, sizeof(struct kif));

	reachable = (flags & IFF_UP) && (flags & IFF_RUNNING);
	if (reachable == kif->k.nh_reachable)
		return (0);

	kif->k.nh_reachable = reachable;

	/* update redistribute list */
	RB_FOREACH(kr, kroute_tree, &krt) {
		for (tkr = kr; tkr != NULL; tkr = tkr->next) {
			if (tkr->r.ifindex == ifindex) {
				if (reachable)
					tkr->r.flags &= ~F_DOWN;
				else
					tkr->r.flags |= F_DOWN;
			}
		}
		kr_redistribute(kr);
	}

	return (0);
}

int
dellink(struct nlmsghdr *nlmsg)
{
	struct ifinfomsg	*ifi;
	struct kif_node		*kif;

	if (nlmsg->nlmsg_len < NLMSG_LENGTH(sizeof(*ifi)))
		return (-1);

	ifi = NLMSG_DATA(nlmsg);
	kif = kif_find(ifi->ifi_index);
	kif_remove(kif);

	return (0);
}

static int
kr_add(struct in_addr prefix, struct in_addr nexthop, int ifindex,
	  int mtu, int flags, int prio)
{
	struct kroute_node	*kr;

	kr = calloc(1, sizeof(*kr));
	if (kr == NULL) {
		log_warn("kr_add: calloc");
		return (-1);
	}

	kr->r.flags = flags;
	kr->r.priority = prio;
	kr->r.prefix = prefix;
	kr->r.nexthop = nexthop;
	kr->r.ifindex = ifindex;
	kr->r.mtu = mtu;
	kroute_insert(kr);

	return (0);
}

int
newroute(struct nlmsghdr *nlmsg)
{
	struct rtmsg		*rtm;
	struct rtattr		*tb[RTA_MAX + 1];
	struct rtattr		*tbn[RTA_MAX + 1];
	struct rtattr		*rta;
	struct kroute_node	*kr, *okr;
	struct in_addr		 src, dst, gw;
	int			 ifindex, mtu = 0, len, rv;
	int			 flags;
	u_int8_t		 prio;

	rtm = NLMSG_DATA(nlmsg);
	len = nlmsg->nlmsg_len - NLMSG_LENGTH(sizeof(*rtm));
	if (len < 0)
		return (-1);

	if (rtm->rtm_family != AF_INET)
		return (0);

	if (rtm->rtm_type != RTN_UNICAST)
		return (0);

	if (rtm->rtm_table != RT_TABLE_MAIN)
		return (0);

	if (rtm->rtm_src_len != 0)
		return (0);

	if (rtm->rtm_flags & RTM_F_CLONED)
		return (0);

	prio = rtm->rtm_protocol;
	switch (prio) {
	case RTPROT_REDIRECT:
	case RTPROT_KERNEL:
		return (0);
	case RTPROT_STATIC:
		flags = F_STATIC;
		break;
	case RTPROT_OSPF:
		flags = F_OSPFD_INSERTED;
		break;
	default:
		flags = F_KERNEL;
	}

	switch (rtm->rtm_type) {
	case RTN_BLACKHOLE:
		flags |= F_BLACKHOLE;
		break;
	case RTN_UNREACHABLE:
		flags |= F_REJECT;
		break;
	}

	if (rtm->rtm_scope == RT_SCOPE_LINK)
		flags |= F_CONNECTED;

	rtattr_parse(RTM_RTA(rtm), len, tb, RTA_MAX);

	dst.s_addr = rtattr_get_u32(tb[RTA_DST]);
	if (dst.s_addr == INADDR_ANY)
		return (0);

	src.s_addr = rtattr_get_u32(tb[RTA_PREFSRC]);
	gw.s_addr = rtattr_get_u32(tb[RTA_GATEWAY]);
	ifindex = rtattr_get_u32(tb[RTA_OIF]);

	rta = tb[RTA_METRICS];
	if (rta) {
		rtattr_parse_nested(rta, tbn, RTA_MAX);
		mtu = rtattr_get_u32(tbn[RTAX_MTU]);
	}

	okr = kroute_find(dst.s_addr, rtm->rtm_dst_len, prio);
	if (okr == NULL)
		return kr_add(dst, gw, ifindex, mtu, flags, prio);

	rta = tb[RTA_MULTIPATH];
	if (rta) {
		struct rtnexthop	*nh;
		struct in_addr		 addr;
		int			 nlen;

		len = RTA_PAYLOAD(rta);
		for (nh = RTA_DATA(rta); RTNH_OK(nh, len); nh = RTNH_NEXT(nh)) {
			ifindex = nh->rtnh_ifindex;
			nlen = nh->rtnh_len - sizeof(*nh);
			if (nlen <= 0) {
				log_warnx("newroute: missing multipath nexthop");
				continue;
			}
			rtattr_parse(RTNH_DATA(nh), nlen, tbn, RTA_MAX);
			addr.s_addr = rtattr_get_u32(tbn[RTA_GATEWAY]);
			rv = kr_add(dst, addr, ifindex, mtu, flags, prio);
			if (rv < 0)
				return (rv);
		}
	}

	kr = kroute_matchgw(okr, gw);
	if (rta && kr == NULL) {
		log_warnx("newroute: multipath route not found");
		return kr_add(dst, gw, ifindex, mtu, flags, prio);
	}

	if (kr->r.flags & F_REDISTRIBUTED)
		flags |= F_REDISTRIBUTED;
	kr->r.nexthop = gw;
	kr->r.src = src;
	kr->r.flags = flags;
	kr->r.ifindex = ifindex;

	rtlabel_unref(kr->r.rtlabel);
	kr->r.rtlabel = 0;
	kr->r.ext_tag = 0;

	if (kif_validate(kr->r.ifindex))
		kr->r.flags &= ~F_DOWN;
	else
		kr->r.flags |= F_DOWN;

	/* just read, the RDE will care */
	kr->serial = kr_state.fib_serial;
	kr_redistribute(okr);

	return (0);
}

int
delroute(struct nlmsghdr *nlmsg)
{
	struct rtmsg		*rtm;
	struct rtattr		*tb[RTA_MAX + 1];
	struct kroute_node	*kr;
	struct in_addr		 dst;
	int			 len;
	u_int8_t		 prio;

	rtm = NLMSG_DATA(nlmsg);
	len = nlmsg->nlmsg_len - NLMSG_LENGTH(sizeof(*rtm));
	if (len < 0)
		return (-1);

	if (rtm->rtm_family != AF_INET)
		return (0);

	if (rtm->rtm_type != RTN_UNICAST)
		return (0);

	if (rtm->rtm_table != RT_TABLE_MAIN)
		return (0);

	if (rtm->rtm_src_len != 0)
		return (0);

	if (rtm->rtm_flags & RTM_F_CLONED)
		return (0);

	prio = rtm->rtm_protocol;
	rtattr_parse(RTM_RTA(rtm), len, tb, RTA_MAX);
	dst.s_addr = rtattr_get_u32(tb[RTA_DST]);
	kr = kroute_find(dst.s_addr, rtm->rtm_dst_len, prio);
	if (kr == NULL)
		return (0);

	return kroute_remove(kr);
}

int
send_rtmsg(int action, struct kroute *kroute)
{
	struct sockaddr_nl	 snl;
	struct iovec		 iov;
	struct msghdr		 msg;
	struct nlmsghdr		*nlmsg;
	struct rtmsg		*rtm;
	char			 buf[RT_BUF_SIZE];
	int			 rv = 0;

	if (kr_state.fib_sync == 0)
		return (0);

	bzero(buf, sizeof(buf));
	iov.iov_base = buf;
	iov.iov_len = sizeof(buf);

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	bzero(&msg, sizeof(msg));
	msg.msg_name = &snl;
	msg.msg_namelen = sizeof(snl);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	nlmsg = (struct nlmsghdr *)buf;
	bzero(nlmsg, sizeof(*nlmsg));
	nlmsg->nlmsg_len = NLMSG_LENGTH(sizeof(*rtm));
	nlmsg->nlmsg_type = action;
	nlmsg->nlmsg_flags = NLM_F_CREATE | NLM_F_REPLACE | NLM_F_REQUEST;
	nlmsg->nlmsg_pid = 0;	/* kernel */
	nlmsg->nlmsg_seq = ++kr_state.rtseq;

	rtm = NLMSG_DATA(nlmsg);
	rtm->rtm_family = AF_INET;
	rtm->rtm_table = RT_TABLE_MAIN;
	rtm->rtm_dst_len = kroute->prefixlen;
	rtm->rtm_protocol = RTPROT_OSPF;
	rtm->rtm_scope = kroute->nexthop.s_addr == INADDR_ANY ?
	    RT_SCOPE_LINK : RT_SCOPE_UNIVERSE;

	if (action == RTM_NEWROUTE) {
		if (kroute->flags & F_BLACKHOLE)
			rtm->rtm_type = RTN_BLACKHOLE;
		else if (kroute->flags & F_REJECT)
			rtm->rtm_type = RTN_UNREACHABLE;
		else
			rtm->rtm_type = RTN_UNICAST;
	}

	rv = rtattr_add(nlmsg, sizeof(buf), RTA_DST, &kroute->prefix,
	    sizeof(kroute->prefix));
	if (rv < 0)
		return (rv);

	rv = rtattr_add(nlmsg, sizeof(buf), RTA_PRIORITY, &kroute->metric,
	    sizeof(kroute->metric));
	if (rv < 0)
		return (rv);

	if (kroute->mtu) {
		char		 rta_buf[RT_BUF_SIZE];
		struct rtattr	*rta = (void *)rta_buf;

		rta->rta_len = RTA_LENGTH(0);
		rv = rtattr_add_sub(rta, sizeof(rta_buf), RTAX_MTU,
		    &kroute->mtu, sizeof(kroute->mtu));
		if (rv < 0)
			return (rv);
		rv = rtattr_add(nlmsg, sizeof(buf), RTA_METRICS,
		    RTA_DATA(rta), RTA_PAYLOAD(rta));
		if (rv < 0)
			return (rv);
	}

	if (kroute->flags & (F_BLACKHOLE | F_REJECT))
		return sendmsg(kr_state.cmd_fd, &msg, 0);

	rv = rtattr_add(nlmsg, sizeof(buf), RTA_GATEWAY, &kroute->nexthop,
	    sizeof(kroute->nexthop));
	if (rv < 0)
		return (rv);

	if (kroute->src.s_addr != INADDR_ANY) {
		rv = rtattr_add(nlmsg, sizeof(buf), RTA_PREFSRC, &kroute->src,
		    sizeof(kroute->src));
		if (rv < 0)
			return (rv);
	}

	if (kroute->ifindex != 0) {
		rv = rtattr_add(nlmsg, sizeof(buf), RTA_OIF, &kroute->ifindex,
		    sizeof(kroute->ifindex));
		if (rv < 0)
			return (rv);
	}

	return sendmsg(kr_state.cmd_fd, &msg, 0);
}

int
send_rtgenmsg(int fd, int family, int type)
{
	struct sockaddr_nl	 snl;
	struct {
		struct nlmsghdr	 nlmsg;
		struct rtgenmsg	 rtgen;
	}			 req;

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	bzero(&req, sizeof(req));
	req.nlmsg.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtgenmsg));
	req.nlmsg.nlmsg_type = type;
	req.nlmsg.nlmsg_flags = NLM_F_ROOT | NLM_F_MATCH | NLM_F_REQUEST;
	req.nlmsg.nlmsg_pid = kr_state.pid;
	req.nlmsg.nlmsg_seq = ++kr_state.rtseq;
	req.rtgen.rtgen_family = family;

	return sendto(fd, &req, sizeof(req), 0, (struct sockaddr *)&snl, sizeof(snl));
}

int
fetchtable(void)
{

	if (send_rtgenmsg(kr_state.cmd_fd, AF_NETLINK, RTM_GETROUTE) < 0)
		return (-1);

	return recv_nlmsg(kr_state.cmd_fd);
}

int
fetchifs(void)
{

	if (send_rtgenmsg(kr_state.cmd_fd, AF_NETLINK, RTM_GETLINK) < 0)
		return (-1);

	if (recv_nlmsg(kr_state.cmd_fd) < 0)
		return (-1);

	if (send_rtgenmsg(kr_state.cmd_fd, AF_NETLINK, RTM_GETADDR) < 0)
		return (-1);

	if (recv_nlmsg(kr_state.cmd_fd) < 0)
		return (-1);

	return (0);
}

int
recv_nlmsg(int fd)
{
	struct sockaddr_nl	 snl;
	struct iovec		 iov;
	struct msghdr		 msg;
	struct nlmsghdr		*nlmsg;
	char			 buf[RT_BUF_SIZE];
	ssize_t			 n;
	int			 rv = 0;

again:
	iov.iov_base = buf;
	iov.iov_len = sizeof(buf);

	memset(&snl, 0, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	memset(&msg, 0, sizeof(msg));
	msg.msg_name = &snl;
	msg.msg_namelen = sizeof(snl);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	if ((n = recvmsg(fd, &msg, 0)) == -1) {
		if (errno == EINTR)
			goto again;
		if (errno == EAGAIN || errno == EWOULDBLOCK)
			return rv;
		log_warn("recv_nlmsg: recvmsg error");
		return (-1);
	}

	if (n == 0) {
		log_warnx("recv_nlmsg: netlink socket closed");
		return (-1);
	}

	if (msg.msg_flags & MSG_TRUNC) {
		log_warn("recv_nlmsg: truncated message");
		return (-1);
	}

	if (msg.msg_namelen != sizeof(snl)) {
		log_warn("recv_nlmsg: sockaddr_nl invalid length %d",
		    msg.msg_namelen);
		return (-1);
	}

	if (msg.msg_flags & MSG_TRUNC) {
		log_warn("recv_nlmsg: message truncated");
		return (-1);
	}

	nlmsg = (struct nlmsghdr *)buf;
	for (; NLMSG_OK(nlmsg, n); nlmsg = NLMSG_NEXT(nlmsg, n)) {
		if (nlmsg->nlmsg_type == NLMSG_DONE)
			return (rv);

		if (nlmsg->nlmsg_type == NLMSG_ERROR) {
			struct nlmsgerr *err = NLMSG_DATA(nlmsg);

			if (nlmsg->nlmsg_len < NLMSG_SPACE(sizeof(*err))) {
				log_warn("recv_nlmsg: truncated error");
				return (-1);
			}

			if (!err->error) { /* ACK */
				if (!(nlmsg->nlmsg_flags & NLM_F_MULTI))
					return (0);
				continue;
			}
			log_warnx("recv_nlmsg: error:%s type:%u seq:%u pid:%u",
			    strerror(-err->error), err->msg.nlmsg_type,
			    err->msg.nlmsg_seq, err->msg.nlmsg_pid);
		}

		if (dispatch_nlmsg(nlmsg) < 0) {
			log_warnx("recv_nlmsg: failed processing msg %d",
			    nlmsg->nlmsg_type);
			rv = -1;
		}
	}

	if (n) {
		log_warnx("recv_nlmsg: %ld bytes remaining", n);
		return (-1);
	}

	goto again;
}

int
dispatch_nlmsg(struct nlmsghdr *nlmsg)
{

	switch (nlmsg->nlmsg_type) {
	case RTM_NEWLINK:
		return newlink(nlmsg);
	case RTM_DELLINK:
		return dellink(nlmsg);
	case RTM_NEWADDR:
		return newaddr(nlmsg);
	case RTM_DELADDR:
		return deladdr(nlmsg);
	case RTM_NEWROUTE:
		return newroute(nlmsg);
	case RTM_DELROUTE:
		return delroute(nlmsg);
	default:
		return (0);
	}
}
